package utils.Contact_Details;

import utils.ContactDetails;

public class Email extends ContactDetails {

    public Email (String p_value) {
        this.Value = p_value;
        this.goodMessage = "Email recevable";
        this.wrongMessage = "Email Non Recevable";
        this.pattern = "^(.+)@(\\S+)$";
    }
}
