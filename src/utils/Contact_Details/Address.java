package utils.Contact_Details;

import utils.ContactDetails;

public class Address extends ContactDetails {

    public Address (String p_value) {
        this.Value = p_value;
        this.goodMessage = "Adresse recevable";
        this.wrongMessage = "Adresse Non Recevable";
        this.pattern ="([0-9]*) ?([a-zA-Z,\\. ]*) ?([0-9]{5}) ?([a-zA-Z]*)";
    }
}