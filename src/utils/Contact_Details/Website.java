package utils.Contact_Details;

import utils.ContactDetails;

public class Website extends ContactDetails {

    public Website (String p_value) {
        this.Value = p_value;
        this.goodMessage = "URL de site recevable";
        this.wrongMessage = "URL de site Non Recevable";
        this.pattern ="^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~|!:,.;]*[-a-zA-Z0-9+&@#/%=~|]";
    }
}
