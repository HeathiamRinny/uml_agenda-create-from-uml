package utils.Contact_Details;

import utils.ContactDetails;

public class Phone extends ContactDetails {

    public Phone (String p_value) {
        this.Value = p_value;
        this.goodMessage = "Numéro recevable";
        this.wrongMessage = "Numéro Non Recevable";
        this.pattern = "^((\\+)33|0)[1-9](\\d{2}){4}$";
    }

}
