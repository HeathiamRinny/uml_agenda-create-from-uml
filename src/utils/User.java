package utils;

public class User {
    private String login;
    private String password;
    private String jwtToken;
    public Agenda agenda;

    public User(String login, String password, String jwtToken) {
        super();
        this.login = login;
        this.password = password;
        this.jwtToken = jwtToken;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getJwtToken() {
        return jwtToken;
    }

    public void setJwtToken(String jwtToken) {
        this.jwtToken = jwtToken;
    }

    public void AddAgenda(Agenda agenda){
        this.agenda = agenda;
    }
}
