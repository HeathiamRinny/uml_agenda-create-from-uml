package utils;


import java.util.ArrayList;
import java.util.List;

public class Agenda {
    //list contacts
    //void addcontact

    List<Contact> Contacts = new ArrayList<>();

    String name;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Agenda (String name){
        this.name = name;
    }
    public void AddContact(Contact newContact) {
        this.Contacts.add(newContact);
    }
    
    public void ShowAllContacts(){
        for (Contact contact : this.Contacts) {
            System.out.println("Nom du contact : " + contact.getName());
        }
    }
}
