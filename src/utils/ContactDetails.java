package utils;

public abstract class ContactDetails {

    public String Value;
    public String pattern = "Default Pattern";
    public String goodMessage = "Default Good Message";
    public  String wrongMessage = "Default Wrong Message";

    public void setValue(String Value) {
        this.Value = Value;
    }

    public String getValue() {
        return this.Value;
    }

    public void Validate() {

        try {
            if (this.Value.matches(pattern)) {
                System.out.println(this.goodMessage);

            }
            else {
                throw new Exception(this.wrongMessage);

            }

        }
        catch (Exception e) {
            System.out.println(this.wrongMessage);

        }
    }


}
