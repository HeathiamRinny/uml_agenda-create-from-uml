import utils.Agenda;
import utils.Contact;
import utils.Contact_Details.Address;
import utils.Contact_Details.Email;
import utils.Contact_Details.Phone;
import utils.Contact_Details.Website;
import utils.User;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {


        boolean continueAddingContact = true;

        System.out.println("Bonjour ! Nous allons cr�er ton agenda ! :)");
        Scanner scanner = new Scanner(System.in);  // Create a Scanner object


        System.out.println("Commence par �crire ton nom !");

        String inputUserName = scanner.nextLine();  // Read user input

        System.out.println("Maintenant rentre ton mot de passe...");
        String inputUserPsswrd = scanner.nextLine();  // Read user input

        System.out.println("...Et un token ! ");
        String inputUserToken = scanner.nextLine();  // Read user input

        User User = new User(inputUserName, inputUserPsswrd, inputUserPsswrd);

        System.out.println("Hop ! Ton utilisateur a �t� cr��. Tu t'appelles " + User.getLogin() + " ! ");
        System.out.println("On va maintenant cr�er ton agenda ! ");

        System.out.println("Rentre son nom ! :)");
        String inputAgenda = scanner.nextLine();  // Read user input

        Agenda newAgenda = new Agenda(inputAgenda);
        System.out.println(newAgenda.getName());
        User.AddAgenda(newAgenda);

        System.out.println("Ton agenda se nomme : " + newAgenda.getName() + " ! Joli nom !");

        do {
            System.out.println("Maintenant il faut ajouter des contacts, afin qu'il ne soit pas vide !");
            System.out.println("Entre le nom  de ton premier contact. :)");

            String inputContact = scanner.nextLine();  // Read user input

            Contact newContact = new Contact(inputContact);

            System.out.println("Entre son num�ro maintenant...");

            String inputPhone = scanner.nextLine();  // Read user input

            Phone phone = new Phone(inputPhone);
            newContact.addContactDetails(phone);

            System.out.println("...Et son adresse ! ('01 xxx xxxx 12345 xxxx').");

            String inputAddress = scanner.nextLine();  // Read user input

            Address address = new Address(inputAddress);
            newContact.addContactDetails(address);

            System.out.println("Entre son site web aussi... ('https://xxxxxxx').");

            String inputWebsite = scanner.nextLine();  // Read user input

            Website website = new Website(inputWebsite);
            newContact.addContactDetails(website);

            System.out.println("...Et son adresse Email ! ");

            String inputEmail = scanner.nextLine();  // Read user input

            Email email = new Email(inputEmail);
            newContact.addContactDetails(email);

            User.agenda.AddContact(newContact);


            System.out.println("Et voil� ! Ton premier contact " + newContact.getName() + "a �t� ajout� � ton agenda !");
            System.out.println("Veux-tu ajouter un autre contact ? R�ponds par 'Oui', si oui !");

            String answerContinue = scanner.nextLine();  // Read user input

            if(answerContinue.equals("Oui")){
                System.out.println("D'accord, allons-y !");
            }
            else {
                System.out.println("D'accord, merci beaucoup ! Au revoir !");
                continueAddingContact = false;
            }
        }
        while (continueAddingContact);


        System.out.println("Voici tout tes contacts : ");
        User.agenda.ShowAllContacts();

    }

}