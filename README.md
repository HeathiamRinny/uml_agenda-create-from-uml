# UML_Agenda-Create-from-uml



## Pour installer le projet

- Commencez par clonner le projet
- Vérifiez que vous ayez au moins JDK 17 installé sur votre machine. Pour vérifier rendez-vous dans un cmd et tapez :
```shell
java -version
```

Si vous n'avez pas de JDK rendez-vous sur : <https://www.oracle.com/fr/java/technologies/downloads/> et téléchargez la dernière version de JDK
- Rendez-vous ensuite dans les variables d'environnement et allez dans la variable appellée "Path" et ajoutez le chemin du dossier bin 

## Pour lancer le projet
- Rendez-vous dans un terminal et accédez au dossier du projet
- Lancez le fichier phonebook.jar
```shell
java -jar <votre_chemin>\uml_agenda-create-from-uml\phonebook.jar
```

## Voici l'image UML du projet : 

- https://i.goopics.net/v8nw8u.png
